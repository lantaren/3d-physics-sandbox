var game:Game;

function FixedUpdate()
{
	if (game.gameState == GameStates.Editing) 
	{
	
		#if MOBILE_INPUT
			if (Input.GetTouch(0).tapCount > 1)
			{
				Deselect();
				
				game.gui.UpdateUI();
			}
		#endif
	
		if (Input.GetMouseButtonDown(1) || Input.GetButtonDown("Cancel")) 
		{			
			game.selector.Deselect();
			
			game.gui.UpdateUI();
		}
	
	}
	
	#if !MOBILE_INPUT
		if (Input.GetAxis("Mouse X") != 0 || Input.GetAxis("Mouse Y") != 0) 
		{ 
			game.hasController = false; 
		}
		
		if (Input.GetAxis("H. Move") != 0 || Input.GetAxis("V. Move") != 0 && Input.GetAxis("Horizontal") != 0 && Input.GetAxis("Vertical") != 0) 
		{
			game.hasController = true;
		} 	
	#else
		game.hasController=true;
	#endif
}
