using System;

public class GameStates
{
	public const String Paused = "Paused";
	public const String Playing = "Playing";
	public const String Editing = "Editing";
	public const String ConnectedBody = "ConnectedBody";
}