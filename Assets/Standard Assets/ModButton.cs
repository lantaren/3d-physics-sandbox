using UnityEngine;

public class ModButton : UnityEngine.UI.Button
{
	protected override void Start()
	{
		if (this.onClick.GetPersistentEventCount() == 0)
			throw new UnassignedReferenceException(this.name);

		base.Start();
	}
}
