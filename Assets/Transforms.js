#pragma strict

var game:Game;

private var selector:Selector;

function Start()
{
	selector = game.selector;
}

public function EditObjName() 
{
	var str = GameObject.Find("IFname").GetComponent(InputField).text;
	selector.selection.name = str;
}

public function EditObjPos()
{	
	var x:float = float.Parse(GameObject.Find("PositionPanel/IFx").GetComponentInChildren(InputField).text);
	x = x <= 100 ? (x >=-100 ? x : -100) : 100;
	
	var y:float = float.Parse(GameObject.Find("PositionPanel/IFy").GetComponentInChildren(InputField).text);
	y = y <= 100 ? (y >= -1 ? y : -1) : 100;
	
	var z:float = float.Parse(GameObject.Find("PositionPanel/IFz").GetComponentInChildren(InputField).text);
	
	EditObjPos(Vector3(x, y, z));
}

public function EditObjPos(pos:Vector3)
{
	var oldPos = selector.selection.transform.position;
	selector.selection.transform.position = pos;
	selector.selection.GetComponent(ObjectScript).position = pos;
	
	var delta = pos - oldPos;
	
	move(selector.selection, delta);
}

public function EditObjRot() 
{
	var oldRot = selector.selection.transform.rotation.eulerAngles;
	var x:float = float.Parse(GameObject.Find("RotationPanel/IFx").GetComponentInChildren(InputField).text);
	var y:float = float.Parse(GameObject.Find("RotationPanel/IFy").GetComponentInChildren(InputField).text);
	var z:float = float.Parse(GameObject.Find("RotationPanel/IFz").GetComponentInChildren(InputField).text);
	
	selector.selection.transform.eulerAngles = Vector3(x, y, z); 
	selector.selection.GetComponent(ObjectScript).rotation = Vector3(x, y, z);
	
	var delta = selector.selection.transform.rotation.eulerAngles - oldRot;
	
	rotate(selector.selection, delta);
}

public function EditObjScale() 
{
	var oldScale = selector.selection.transform.localScale;
	var x:float = float.Parse(GameObject.Find("ScalePanel/IFx").GetComponentInChildren(InputField).text);
	var y:float = float.Parse(GameObject.Find("ScalePanel/IFy").GetComponentInChildren(InputField).text);
	var z:float = float.Parse(GameObject.Find("ScalePanel/IFz").GetComponentInChildren(InputField).text);
	
	var delta = Vector3(x, y, z) - oldScale;
	
	selector.selection.transform.localScale = Vector3(x, y, z);
	selector.selection.GetComponent(ObjectScript).scale = Vector3(x, y, z);
	
	scale(selector.selection, delta);
}

function move(body:GameObject, position:Vector3) 
{
	for (var jointObject : Joint in body.GetComponents(Joint)) 
	{
		if (jointObject.connectedBody !== null) 
		{ 
			jointObject.connectedBody.transform.position += position;
			jointObject.connectedBody.GetComponent(ObjectScript).position = jointObject.connectedBody.transform.position;
			//jointObject.connectedBody.GetComponent(Renderer).material = conMat;
		}
	}
}

function rotate(body:GameObject, rotation : Vector3) 
{
	for (var jointObject : Joint in body.GetComponents(Joint)) 
	{
		if (jointObject.connectedBody != null) 
			jointObject.connectedBody.transform.Rotate(rotation);
	}
}

function scale(body:GameObject, scale : Vector3) 
{
	for (var jointObject : Joint in body.GetComponents(Joint)) 
	{
		if (jointObject.connectedBody != null)
			jointObject.connectedBody.transform.localScale += scale;
	}
}
