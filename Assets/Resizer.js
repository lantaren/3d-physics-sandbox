#pragma strict

var game:Game;
private var oldScaleRes:float;
private var lastScreenSize:Vector2;

function Start()
{
	oldScaleRes = game.ScaleRes;
	lastScreenSize = Vector2(Screen.width, Screen.height);
}

function Update() 
{ 
	if (lastScreenSize != Vector2(Screen.width, Screen.height) || game.ScaleRes != oldScaleRes) 
	{		
		var bloom:UnityStandardAssets.ImageEffects.BloomOptimized = game.cam.GetComponent(UnityStandardAssets.ImageEffects.BloomOptimized); 
		bloom.blurSize = 2 * game.ScaleRes;
		
		oldScaleRes = game.ScaleRes;
		game.lastScreenSize = Vector2(Screen.width, Screen.height);
		var renTex:RawImage = game.renderTexture.GetComponent(RawImage);
		var nrentex:RenderTexture = Instantiate(renTex.texture);
		nrentex.name = "main";
		nrentex.width = Screen.width * game.ScaleRes;
		nrentex.height= Screen.height* game.ScaleRes;
		//nrentex.useMipMap = true;
		//nrentex.generateMips = true;
		game.cam.targetTexture = nrentex;
		//Debug.Log(nrentex.texelSize);
		renTex.texture = nrentex;
		
		oldScaleRes = game.ScaleRes;
		lastScreenSize = Vector2(Screen.width, Screen.height);	
	}
}