﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

public class MoveCamera : MonoBehaviour 
{
	public float turnSpeed = 4.0f;
	
	private Vector3 mouseOrigin;
	private bool isRotating;

	new public Camera camera;
	
	void Update () 
	{
		if(Input.GetMouseButtonDown(2))
		{
			mouseOrigin = Input.mousePosition;
			isRotating = true;
		}

		if (!Input.GetMouseButton(2)) 
			isRotating=false;

		if (isRotating)
		{
			Vector3 pos = camera.ScreenToViewportPoint(Input.mousePosition - mouseOrigin);
			
			transform.RotateAround(transform.position, transform.right, -pos.y * turnSpeed);
			transform.RotateAround(transform.position, Vector3.up, pos.x * turnSpeed);
		}

		transform.RotateAround (transform.position, transform.right, -Input.GetAxis("Vertical") * turnSpeed/4);
		transform.RotateAround (transform.position, Vector3.up, Input.GetAxis("Horizontal") * turnSpeed/4);

		transform.RotateAround (transform.position, transform.right, -CrossPlatformInputManager.GetAxisRaw("Vertical") * turnSpeed/4);
		transform.RotateAround (transform.position, Vector3.up, CrossPlatformInputManager.GetAxisRaw("Horizontal") * turnSpeed/4);
		
	}
}