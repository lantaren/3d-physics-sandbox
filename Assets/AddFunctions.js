#pragma strict

var game:Game;

public function addCube () 
{
	var cube = game.primitives.transform.FindChild("Cube");
	addShape(cube);
}

public function addSphere() 
{
	var sphere = game.primitives.transform.FindChild("Sphere");
	addShape(sphere);
}

public function addCylinder() 
{
	var cyl = game.primitives.transform.FindChild("Cylinder");
	addShape(cyl);
}

private function addShape(shape:Transform, location:Vector3, rotation:Quaternion)
{
	var instance = transform.Instantiate(shape, location, rotation);
	instance.SetParent(game.world.transform);
	instance.tag = 'selected';
	
	var sel = GameObject.FindGameObjectWithTag('selected');
	sel.GetComponent(ObjectScript).position = sel.transform.position;
	
	return sel;
}

function addShape(shape : Transform) 
{
	var sel = addShape(shape, Vector3(0,0,5), Quaternion.Euler(0,0,0));
	
	game.selector.Select(sel);
}

function addShape(shape : Transform, location : Vector3, rotation : Quaternion, objscript : ObjectScript) 
{
	var sel = addShape(shape, location, rotation);
	sel.GetComponent(ObjectScript).constrainPos = objscript.constrainPos;
	sel.GetComponent(ObjectScript).constrainRot = objscript.constrainRot;
	
	game.selector.Select(sel);
}

public function addFixedJoint() 
{ 
	if (game.selector.selection != null)
		game.selector.selection.AddComponent(FixedJoint);
		
	game.gui.UpdateUI();
}

public function addHingeJoint() 
{ 
	if (game.selector.selection != null)
		game.selector.selection.AddComponent(HingeJoint);
		
	game.gui.UpdateUI(); 
}

public function addSpringJoint() 
{ 
	if (game.selector.selection != null)
		game.selector.selection.AddComponent(SpringJoint);
		
	game.gui.UpdateUI(); 
}
