var game:Game;

public function UpdateUI () 
{
	
	var old = game.gameState;
	gameState = "GUIupdate";
	var objEditAnimator:Animator = GameObject.Find("ObjEdit").GetComponent(Animator);
	var selObj = game.selector.selection;
	
	if (selObj == null) 
	{
		objEditAnimator.SetBool("MenuObjHidden", true);
		GameObject.Find("FixedBtn").GetComponent(ModButton).interactable = false;
		GameObject.Find("HingeBtn").GetComponent(ModButton).interactable = false;
		GameObject.Find("SpringBtn").GetComponent(ModButton).interactable = false;
		
	} 
	else 
	{
		objEditAnimator.SetBool("MenuObjHidden", false);
		
		selObj.GetComponent(Renderer).material = game.selMat;
		
		GameObject.Find("FixedBtn").GetComponent(ModButton).interactable = true;
		GameObject.Find("HingeBtn").GetComponent(ModButton).interactable = true;
		GameObject.Find("SpringBtn").GetComponent(ModButton).interactable = true;
		
		GameObject.Find("PositionPanel/IFx").GetComponentInChildren(InputField).text = selObj.transform.position.x.ToString();
		GameObject.Find("PositionPanel/IFy").GetComponentInChildren(InputField).text = selObj.transform.position.y.ToString();
		GameObject.Find("PositionPanel/IFz").GetComponentInChildren(InputField).text = selObj.transform.position.z.ToString();
		
		GameObject.Find("RotationPanel/IFx").GetComponentInChildren(InputField).text = selObj.transform.eulerAngles.x.ToString();
		GameObject.Find("RotationPanel/IFy").GetComponentInChildren(InputField).text = selObj.transform.eulerAngles.y.ToString();
		GameObject.Find("RotationPanel/IFz").GetComponentInChildren(InputField).text = selObj.transform.eulerAngles.z.ToString();
		
		GameObject.Find("ScalePanel/IFx").GetComponentInChildren(InputField).text = selObj.transform.localScale.x.ToString();
		GameObject.Find("ScalePanel/IFy").GetComponentInChildren(InputField).text = selObj.transform.localScale.y.ToString();
		GameObject.Find("ScalePanel/IFz").GetComponentInChildren(InputField).text = selObj.transform.localScale.z.ToString();
		
		GameObject.Find("PositionFreeze/xTog").GetComponent(Toggle).isOn = selObj.GetComponent(ObjectScript).constrainPos[0];
		GameObject.Find("PositionFreeze/yTog").GetComponent(Toggle).isOn = selObj.GetComponent(ObjectScript).constrainPos[1];
		GameObject.Find("PositionFreeze/zTog").GetComponent(Toggle).isOn = selObj.GetComponent(ObjectScript).constrainPos[2];
		GameObject.Find("RotationFreeze/xTog").GetComponent(Toggle).isOn = selObj.GetComponent(ObjectScript).constrainRot[0];
		GameObject.Find("RotationFreeze/yTog").GetComponent(Toggle).isOn = selObj.GetComponent(ObjectScript).constrainRot[1];
		GameObject.Find("RotationFreeze/zTog").GetComponent(Toggle).isOn = selObj.GetComponent(ObjectScript).constrainRot[2];
		
		var rb : Rigidbody = selObj.GetComponent(Rigidbody);
		GameObject.Find("IFmass").GetComponent(InputField).text = rb.mass.ToString();
		GameObject.Find("DragPanel/IF").GetComponent(InputField).text = rb.drag.ToString();
		GameObject.Find("AngDragPanel/IF").GetComponent(InputField).text = rb.angularDrag.ToString();
		GameObject.Find("StaticTog").GetComponent(Toggle).isOn = rb.isKinematic;
		GameObject.Find("GravityTog").GetComponent(Toggle).isOn = rb.useGravity;
		
		if (game.selJoint != null) 
		{
			var t = game.selJoint.GetType();
			var pan : GameObject;
			
			switch (t) {
				case FixedJoint:
					pan = GameObject.Find("JointFixed/Content/Scroll area");
				break;
				case HingeJoint: 
					pan = GameObject.Find("JointHinge/Content/Scroll area");
				break;
				case SpringJoint:
					pan = GameObject.Find("JointSpring/Content/Scroll area");
				break;
				default:
					pan = GameObject.Find("JointFixed/Content/Scroll area");
				break;
			}	
		}
	
		var jointParent = GameObject.Find("Joints/Content/Scroll area");
		var j = selObj.GetComponents(Joint);
		
		for (var child : Transform in jointParent.transform) 
		{
			GameObject.Destroy(child.gameObject);
		
			if (j.Length > 0) 
			{
				var trans : Transform = game.primitives.transform.FindChild("JointPrefab");
				for(var i = 0; i<j.Length; i++) 
				{
					var jType = j[i].GetType();
					var jso : Joint = j[i];
					var trano = transform.Instantiate(trans);
					var btn:ModButton = trano.FindChild("Go").GetComponent(ModButton);
				  
					switch (jType) {
						case FixedJoint:
							trano.SetParent(jointParent.transform);
							trano.name = "Fixed Joint";
							trano.FindChild("Text").GetComponent(Text).text = "Fixed Joint";
							btn.onClick.AddListener(function () { new game.sub2Switch("Fixed", jso); } );
						break;	
						case HingeJoint:
							trano.SetParent(jointParent.transform);
							trano.name = "Hinge Joint";
							trano.FindChild("Text").GetComponent(Text).text = "Hinge Joint";
							btn.onClick.AddListener(function () { new game.sub2Switch("Hinge", jso); } );
						break;
						case SpringJoint:
							trano.SetParent(jointParent.transform);
							trano.name = "Spring Joint";
							trano.FindChild("Text").GetComponent(Text).text = "Spring Joint";
							btn.onClick.AddListener(function () { new game.sub2Switch("Spring", jso); } );
						break;
						default:
							Debug.Log("Other Joint Detected?");
						break;	
				  	}
		 	    };
		 	}
		}
		selObj.GetComponent(ObjectScript).joints = j;
		Debug.Log(j);
		Debug.Log(selObj.GetComponent(ObjectScript).joints);
	}
	
	if (old == GameStates.ConnectedBody) 
	{
		for (var b : ModButton in GameObject.Find("Header").GetComponentsInChildren(ModButton)) 
		{
			b.interactable = false;
		}
	} 
	else 
	{
		for (var b : ModButton in GameObject.Find("Header").GetComponentsInChildren(ModButton)) 
		{
			b.interactable = true;
		}
	}
	gameState = old;
}

public function SetTimeScale () {
	game.timeScale = GameObject.Find("SimSpeed").GetComponentInChildren(Slider).value /5;
}

function toggleMoveable () 
{ 
	game.moveable = GameObject.Find("SelectToggle").GetComponent(Toggle).isOn;
}

function toggleSelectable () 
{
	game.selectTog = GameObject.Find("SelectToggle 2").GetComponent(Toggle).isOn;
}