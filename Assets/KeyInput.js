#pragma strict

var game:Game;

private var selector:Selector;

function Start()
{
	selector = game.selector;
}

function FixedUpdate ()
{
	if (Input.GetButtonDown("Cancel"))
	{
		EventSystems.EventSystem.current.SetSelectedGameObject(null);
		
		if (game.gameState == "connectedBody") 
		{
			game.gameState = "Editing";
			game.gui.UpdateUI();
		}
	}
	
	if (Input.GetButtonDown("Select")) {
		EventSystems.EventSystem.current.SetSelectedGameObject(GameObject.Find("CubeBtn"));
	}
	
	if (!UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject() && game.gameState == "Editing") 
	{
		if (EventSystems.EventSystem.current.currentSelectedGameObject == null) 
		{
			game.overUI=false;
			if (Input.GetKeyDown(KeyCode.Alpha1)) game.addFunctions.addCube(); 
			if (Input.GetKeyDown(KeyCode.Alpha2)) game.addFunctions.addSphere();
			if (Input.GetKeyDown(KeyCode.Alpha3)) game.addFunctions.addCylinder();
			
			if (selector.selection != null) 
			{
				if (Input.GetKeyDown(KeyCode.Alpha4)) game.addFunctions.addFixedJoint ();
				if (Input.GetKeyDown(KeyCode.Alpha5)) game.addFunctions.addHingeJoint ();
				if (Input.GetKeyDown(KeyCode.Alpha6)) game.addFunctions.addSpringJoint ();
			}
			
			if (Input.GetKeyDown(KeyCode.C) && selector.selection != null) 
				game.copyObj = selector.selection; 
	
			if (Input.GetKeyDown(KeyCode.V)) 
			{ 
				if (game.copyObj != null) 
				{ 
					game.addFunctions.addShape(game.copyObj.transform, game.copyObj.transform.position, game.copyObj.transform.rotation, game.copyObj.GetComponent(ObjectScript)); 
				} 
			}
		}
	}
	
	if (Input.GetKeyDown(KeyCode.Delete) && selector.selection != null && game.gameState == "Editing")
	{		
		Destroy(selector.selection); 
				
		selector.selection = null;
		
		game.gui.UpdateUI();
	}
}