#pragma strict

var selection:GameObject = null;
var game:Game;

function FixedUpdate ()
{	
	if (game.gameState == GameStates.Editing || game.gameState == GameStates.ConnectedBody) 
	{
		
		//if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject()==false) {
		if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject() && game.gameState != GameStates.Editing) 
		{ 
		
		} 
		
		//hasController=(Input.GetMouseButton(0)=null);
		var ray : Ray;
		
		#if !MOBILE_INPUT
			if (game.hasController) 
			{ 
				ray = game.cam.ScreenPointToRay(Vector3(Screen.width/2,Screen.height/2, 0)*game.ScaleRes); 
			} 
			else 
			{ 
				ray = game.cam.ScreenPointToRay(Input.mousePosition*game.ScaleRes); 
			}
		#else
			ray = game.cam.ScreenPointToRay(Input.GetTouch(0).position * ScaleRes);
		#endif
		
		var hit : RaycastHit;
		var mask = ~(1 << LayerMask.NameToLayer ("gameObj"));
		
		if (Physics.Raycast (ray, hit, 100, mask))
		{
			var CursorPos = Vector3(Mathf.RoundToInt(hit.point.x + 0.5 * hit.normal.x),
									Mathf.RoundToInt(hit.point.y + 0.5 * hit.normal.y),
									Mathf.RoundToInt(hit.point.z + 0.5 * hit.normal.z));
			
			if (selection != hit.collider.gameObject) 
			{
				if (Input.GetButtonDown("SelectObj") && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
				{			
					if (game.gameState == GameStates.Editing) 
					{
						if (hit.transform.tag == "level" && game.selectTog) 
							Select(hit.collider.gameObject);
					} 
					else 
					{
						if (hit.transform.tag == "level" && game.selJoint != null) 
						{
							game.selJoint.connectedBody = hit.collider.attachedRigidbody; 
							game.gameState="Editing"; 
							game.gui.UpdateUI();
						}
					}
				}
			} 
			else 
			{
			
			}
			#if !MOBILE_INPUT
			if (Input.GetButton("SelectObj") && game.moveable && game.selectTog && game.gameState == GameStates.Editing)
			{
				if (!UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject()) 
				{
					if (selection != null) 
					{
						game.transfFuncs.EditObjPos(CursorPos);
						//selObj.GetComponent(ObjectScript).position=CursorPos;
						game.gui.UpdateUI();
					}
				}
			}
			#else
			if (game.moveable && game.selectTog && game.gameState==GameStates.Editing) 
			{
				if (selection != null) 
				{
					game.transFuncs.EditObjPos(CursorPos);
					//selObj.GetComponent(ObjectScript).position=CursorPos;
					UpdateUI();
				}
			}
			#endif
		
			if (game.litObj != hit.collider.gameObject && game.litObj != null && (selection == null || game.gameState != GameStates.Editing)) {
				
				game.litObj.transform.GetComponent(Renderer).material = game.stdMat; 
				
				if (hit.transform.tag == "level") 
				{ 
					game.litObj = hit.collider.gameObject; 
					game.litObj.transform.GetComponent(Renderer).material = game.outMat;
				} 
				else 
				{ 
					game.litObj = null; 
				}
			} 
			else if (hit.transform.tag == "level" && selection == null) 
			{
				#if !MOBILE_INPUT
					if (Input.GetButton("SelectObj")) 
					{ 
						Select(hit.collider.gameObject);
			  			GameObject.Find("IFname").GetComponent(InputField).text = selection.name;
					}
				#else
					if (Input.GetTouch(0).tapCount > 1) 
					{ 
						Select(hit.collider.gameObject); 
						GameObject.Find("IFname").GetComponent(InputField).text = selection.name; 
					}
				#endif
				
				hit.transform.GetComponent(Renderer).material = game.outMat; 
			}
		
		} 
		else 
		{ 
				//game.litObj.transform.GetComponent(Renderer).material = game.stdMat;
				game.litObj = null;;
		}
	
	}
}

function Select(gObj:GameObject) {
	Deselect();
	
	selection = gObj;
	selection.transform.tag = "level";
	game.ObjEdit.SetActive(true);
	
	if (!game.selectTog) 
	{ 
		selection = null; 
	}
	else
	{
		selection.layer = LayerMask.NameToLayer("gameObj");
		GameObject.Find("IFname").GetComponent(InputField).text = selection.name;
	}
	
	game.gui.UpdateUI();
}

public function Deselect()
{
	if (selection != null) 
	{ 
		selection.layer = LayerMask.NameToLayer("Default");
		selection.GetComponent(Renderer).material = game.stdMat;
		selection = null;
	}
}