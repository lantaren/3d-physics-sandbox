﻿#pragma strict

public var type : String;
public var position : Vector3 = Vector3(0,0,0);
public var rotation : Vector3 = Vector3(0,0,0);
public var scale : Vector3 = Vector3(1,1,1);
public var constrainPos : Array = [true, true, true];
public var constrainRot : Array = [true, true, true];
public var joints : Component[];
function Start () {
	
}

function Update () {
	//Debug.Log(GetComponent.<Rigidbody>().constraints);
}

function constrain() {
	var px = RigidbodyConstraints.FreezePositionX; var py = RigidbodyConstraints.FreezePositionY; var pz = RigidbodyConstraints.FreezePositionZ;
	var rx = RigidbodyConstraints.FreezeRotationX; var ry = RigidbodyConstraints.FreezeRotationY; var rz = RigidbodyConstraints.FreezeRotationZ;
	var sum = RigidbodyConstraints.None;
	if (constrainPos[0]==true) sum = sum | px;
	if (constrainPos[1]==true) sum = sum | py;
	if (constrainPos[2]==true) sum = sum | pz;
	if (constrainRot[0]==true) sum = sum | rx;
	if (constrainRot[1]==true) sum = sum | ry;
	if (constrainRot[2]==true) sum = sum | rz;
	return sum;
	//GetComponent.<Rigidbody>().constraints =
}