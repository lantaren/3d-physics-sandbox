﻿#pragma strict

import UnityEngine.UI;

var cam:Camera;
var primitives:GameObject;
var world:GameObject;
var stdMat:Material;
var selMat:Material;
var outMat:Material;
var conMat:Material;
var selector:Selector;
var gui:UI;
var litObj : GameObject;
var moveable : boolean = true;
var selectTog: boolean = true;
var cutObj : GameObject = null;
var copyObj : GameObject = null;
var objScript;
var gameState : String = "Editing";
var buildscreen : RectTransform;
var timeScale :float = 1.0f;
var overUI;
var ScaleRes : float = 1.0f;
var renderTexture : GameObject;
var lastScreenSize : Vector2;
var hasController : boolean;
var ObjEdit : GameObject;
var selJoint : Joint;
var addFunctions:AddFunctions;
var transfFuncs:Transforms;

public function play () {
	if (gameState == "Editing")
	{
		GameObject.Find("PlayBtn").GetComponent(Button).interactable = false;
		GameObject.Find("PauseBtn").GetComponent(Button).interactable = true;
		GameObject.Find("StopBtn").GetComponent(Button).interactable = true;
		
		for (var button : Button in GameObject.Find("AddPanel").GetComponentsInChildren(Button)) 
			button.interactable = false;
		
		var length = world.FindGameObjectsWithTag("level").Length;
		
		var bsa:Animator = GameObject.Find("BuildScreen").GetComponent(Animator);
		
		Time.timeScale = 0;
		
		selector.Deselect();
		
		gui.UpdateUI();
			
		if (length > 0) {
			bsa.SetBool("Visible", true);
			gameState = "Playing";
			playroutine(length, bsa);
		}
	}
	
	if (gameState == "Paused")
	{
		GameObject.Find("PauseBtn").GetComponent(Button).interactable = true;
		GameObject.Find("PlayBtn").GetComponentInChildren(UI.Text).text = "Play";
		GameObject.Find("PlayBtn").GetComponent(Button).interactable = false;
		Time.timeScale = 1 * timeScale;
	}
}

function playroutine(ln:int, animate:Animator)
{
	var i = 1;
	
	for (var lvlObj : GameObject in world.FindGameObjectsWithTag("level")) 
	{
			
		lvlObj.GetComponent.<Rigidbody>().constraints = lvlObj.GetComponent(ObjectScript).constrain();
			
		Debug.Log(lvlObj.name + i + " / " + ln);
			
		if (i >= ln) {
				
			Time.timeScale = 1 * timeScale;
			animate.SetBool("Visible", false);
			
		}
		i++;
		yield;
	}
}

public function pause()
{
	GameObject.Find("PlayBtn").GetComponent(Button).interactable = true;
	GameObject.Find("PlayBtn").GetComponentInChildren(UI.Text).text = "Resume";
	GameObject.Find("PauseBtn").GetComponent(Button).interactable = false;
	
	gameState = "Paused";
	Time.timeScale = 0;
}

public function stop()
{
	for (var lvlObj : GameObject in world.FindGameObjectsWithTag("level")) 
	{
		lvlObj.GetComponent.<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
		lvlObj.transform.position = lvlObj.GetComponent(ObjectScript).position;
		lvlObj.transform.rotation = Quaternion.Euler(lvlObj.GetComponent(ObjectScript).rotation);
	}
	
	for (var button : Button in GameObject.Find("AddPanel").GetComponentsInChildren(Button)) 
		button.interactable = true;
		
	GameObject.Find("PlayBtn").GetComponentInChildren(UI.Text).text = "Play";
	Time.timeScale = 1;
	GameObject.Find("PlayBtn").GetComponent(Button).interactable = true;
	GameObject.Find("PauseBtn").GetComponent(Button).interactable = false;
	GameObject.Find("StopBtn").GetComponent(Button).interactable = false;
	gameState = "Editing";
}

function SetConstraints(selection:GameObject)
{
	if (gameState == "Editing" && selection != null) 
	{
		var pX = GameObject.Find("PositionFreeze/xTog").GetComponent(Toggle).isOn;
		var pY = GameObject.Find("PositionFreeze/yTog").GetComponent(Toggle).isOn;
		var pZ = GameObject.Find("PositionFreeze/zTog").GetComponent(Toggle).isOn;
		var rX = GameObject.Find("RotationFreeze/xTog").GetComponent(Toggle).isOn;
		var rY = GameObject.Find("RotationFreeze/yTog").GetComponent(Toggle).isOn;
		var rZ = GameObject.Find("RotationFreeze/zTog").GetComponent(Toggle).isOn;
		
		var obj = selector.selection.GetComponent(ObjectScript);
		obj.constrainPos = [pX, pY, pZ];
		obj.constrainRot = [rX, rY, rZ];
		
		Debug.Log(obj.constrain());
	}
}

function Update ()
{
	
}

public function ScaleResChange() 
{ 
	ScaleRes = GameObject.Find("SSmul/Panel/Slider").GetComponent(Slider).value; 
}

public function QualityLevelChange()
{

}

public function ShadowChange() 
{ 
	Debug.Log(UnityEngine.QualitySettings.shadowDistance); 
	UnityEngine.QualitySettings.shadowDistance = GameObject.Find("Shadow/Panel/Slider").GetComponent(Slider).value; 
}

public function PhysicsChange() 
{
	if (selector.selection != null)
	{
		var rb : Rigidbody = selector.selection.GetComponent(Rigidbody);
		rb.mass = float.Parse(GameObject.Find("IFmass").GetComponent(InputField).text);
		rb.drag = float.Parse(GameObject.Find("DragPanel/IF").GetComponent(InputField).text);
		rb.angularDrag=float.Parse(GameObject.Find("AngDragPanel/IF").GetComponent(InputField).text);
		rb.isKinematic = GameObject.Find("StaticTog").GetComponent(Toggle).isOn;
		rb.useGravity = GameObject.Find("GravityTog").GetComponent(Toggle).isOn;
	}
}

public function FixedJointChange() 
{

}
public function HingeJointChange() 
{

}
public function SpringJointChange() 
{

}

function connectedBody() {
	gameState = "connectedBody";
	gui.UpdateUI();
}

function sub2Switch (jointType:String, j:Joint) 
{
	var sub2:Animator = GameObject.Find("Sub 2").GetComponent(Animator);
	selJoint=j;
	sub2.SetTrigger(jointType);
	var jParent : GameObject = GameObject.Find("Joint"+jointType);
	
	switch (jointType) {
		case "Fixed":
			jParent.transform.Find("");
		break;
		case "Hinge":
		
		break;
		case "Spring":
		
		break;
		default:
			
		break;
	}
}